//
//  accountViewController.swift
//  SnapVideo
//
//  Created by Carlos Osorio on 25/7/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import UIKit
import GoogleSignIn

class accountViewController: UIViewController, GIDSignInUIDelegate {

    
    @IBOutlet weak var wlcomeLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        refreshInterface()
        
    }

    func refreshInterface(){
        if let currentUser = GIDSignIn.sharedInstance().currentUser{
            wlcomeLabel.text="Hi, \(currentUser.profile.name!)!"
            let profilePicURL = currentUser.profile.imageURL(withDimension: 175)
            do {
                let dataImage = try Data(contentsOf: profilePicURL!, options: [])
                profileImage.image = UIImage(data: dataImage)
                profileImage.isHidden = false
                print(currentUser.profile.name)
            } catch {
                
            }
            
            emailLabel.text = currentUser.profile.email
            nameLabel.text = currentUser.profile.givenName

        }
        else{
            wlcomeLabel.text="Hi, stranger!"
            print("you are an stranger")
        }
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut()
        dismiss(animated: true, completion: nil)
    }
    
}
