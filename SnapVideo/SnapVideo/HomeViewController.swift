//
//  HomeViewController.swift
//  SnapVideo
//
//  Created by Carlos Osorio on 17/7/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import UIKit
import GoogleSignIn
import YouTubePlayer_Swift

class HomeViewController: UIViewController {

   
    @IBOutlet weak var videoView: YouTubePlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadPlaylist()
        loadVideo()
        
    }
    
    
    func loadPlaylist(){
        videoView.loadPlaylistID("PLFgquLnL59akMulhC0nleJkYAmP_p-wcG")
    }
    
    func loadVideo(){
        videoView.loadVideoID("rQA5YM9UDrg")
        videoView.playerVars = ["playsinline": 1 as AnyObject, "autoplay": 0 as AnyObject]

    }
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        videoView.nextVideo()
        loadVideoProperty()
    }
    
    @IBAction func dislikeButtonPressed(_ sender: Any) {
        videoView.nextVideo()
        loadVideoProperty()
    }
    
    func loadVideoProperty(){
        videoView.playerVars = ["playsinline": 1 as AnyObject, "autoplay": 0 as AnyObject]
    }
    
    
}
