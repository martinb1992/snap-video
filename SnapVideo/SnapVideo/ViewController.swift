//
//  ViewController.swift
//  SnapVideo
//
//  Created by Martin Balarezo on 18/6/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import UIKit
import GoogleSignIn

class ViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        GIDSignIn.sharedInstance().delegate = self

        GIDSignIn.sharedInstance().uiDelegate = self

        
//
//        //var error: NSError?
//        let signInButton = GIDSignInButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
//        signInButton.center = view.center
//
//        view.addSubview(signInButton)
        
    }
    

    @IBAction func customLoginPressed(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (GIDSignIn.sharedInstance().hasAuthInKeychain()){
            print("signed in")
            performSegue(withIdentifier: "toHomeSegue", sender: self)
            
        }
        else {
            print("not signed in")
            
        }
    }

}

